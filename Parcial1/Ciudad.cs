﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parcial1
{
    public class Ciudad : Descubrimiento
    {
        private string _nombreAntiguo;
        public string NombreAntiguo {
            get { return _nombreAntiguo; }
        }

        private int _intervaloPoblacionActiva;
        public int IntervaloPoblacionActiva {
            get { return _intervaloPoblacionActiva; }
        }

        private int _cantidadHabitantes;
        public int cantidadHabitantes {
            get { return _cantidadHabitantes; }
        }

        public Ciudad(string pais, string ciudad, DateTime fecha,
            string nombreAntiguo, int intervaloPoblacionActiva, int cantidadHabitantes) : base(pais, ciudad, fecha)
        {
            _nombreAntiguo = nombreAntiguo;
            _intervaloPoblacionActiva = intervaloPoblacionActiva;
            _cantidadHabitantes = cantidadHabitantes;
        }

        public override float ObtenerPresupuestoExcavacion()
        {
            return Presupuesto * 0.45f;
        }

        public override float ObtenerPresupuestoMateriales()
        {
            return Presupuesto * 0.35f;
        }

        public override float ObtenerPresupuestoPermisos()
        {
            return Presupuesto * 0.20f;
        }

        public override string ObtenerTipo()
        {
            return "Ciudad";
        }

        public override string ToString()
        {
            return $"Ciudad ({NombreAntiguo}) ({IntervaloPoblacionActiva}) ({cantidadHabitantes})";
        }

        public override string ObtenerDetalles()
        {
            return base.ObtenerDetalles() + "\n" +
                "Nombre antiguo: " + NombreAntiguo + "\n" +
                "Intervalo poblacion activa: " + IntervaloPoblacionActiva + "\n" +
                "Cantidad habitantes: " + cantidadHabitantes + "\n";
        }
    }
}