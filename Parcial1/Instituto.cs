﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parcial1
{
    public class Instituto
    {
        private List<Arqueologo> _arqueologos = new List<Arqueologo>();
        public List<Arqueologo> Arqueologos
        {
            get { return _arqueologos; }
        }

        private List<Descubrimiento> _descubrimientos = new List<Descubrimiento>();
        public List<Descubrimiento> Descubrimientos {
            get { return _descubrimientos; }
        }

        public Instituto()
        {

        }

        public void NuevoDescubrimiento(Descubrimiento descubrimiento)
        {
            descubrimiento.Arqueologo.SumarDescubrimiento();
            _descubrimientos.Add(descubrimiento);
        }

        public void EliminarDescubrimiento(Descubrimiento descubrimiento)
        {
            descubrimiento.Arqueologo.RestarDescubrimiento();
            _descubrimientos.Remove(descubrimiento);
        }

        // d) Obtener el descubrimiento que haya obtenido más presupuesto en excavación
        public Descubrimiento ObtenerDescubrimientoMayorPresupuestoExcavacion()
        {
            Descubrimiento descubrimiento = null;
            foreach(Descubrimiento d in _descubrimientos)
            {
                if (descubrimiento == null ||
                    d.ObtenerPresupuestoExcavacion() > descubrimiento.ObtenerPresupuestoExcavacion())
                {
                    descubrimiento = d;
                }
            }

            return descubrimiento;
        }

        // e) Obtener el descubrimiento que haya asignado más presupuesto en materiales
        public Descubrimiento ObtenerDescubrimientoMayorPresupuestoMateriales()
        {
            Descubrimiento descubrimiento = null;
            foreach (Descubrimiento d in _descubrimientos)
            {
                if (descubrimiento == null ||
                    d.ObtenerPresupuestoMateriales() > descubrimiento.ObtenerPresupuestoMateriales())
                {
                    descubrimiento = d;
                }
            }

            return descubrimiento;
        }

        // f) Obtener el descubrimiento qué menos dinero haya abonado en permisos
        public Descubrimiento ObtenerDescubrimientoMenorPresupuestoPermisos()
        {
            Descubrimiento descubrimiento = null;
            foreach (Descubrimiento d in _descubrimientos)
            {
                if (descubrimiento == null ||
                    d.ObtenerPresupuestoPermisos() < descubrimiento.ObtenerPresupuestoPermisos())
                {
                    descubrimiento = d;
                }
            }

            return descubrimiento;
        }

        // g) Obtener el descubrimiento más antiguo
        public Descubrimiento ObtenerDescubrimientoMasAntiguo()
        {
            Descubrimiento descubrimiento = null;
            foreach (Descubrimiento d in _descubrimientos)
            {
                if (descubrimiento == null ||
                    d.Fecha < descubrimiento.Fecha)
                {
                    descubrimiento = d;
                }
            }

            return descubrimiento;
        }

        // h) Obtener el arqueólogo qué más descubrimientos haya realizado
        public Arqueologo ObtenerArqueologoMasDescubrimientos()
        {
            Arqueologo arqueologo = null;
            foreach (Arqueologo arq in _arqueologos)
            {
                if (arqueologo == null ||
                    arq.Descubrimientos > arqueologo.Descubrimientos)
                {
                    arqueologo = arq;
                }
            }

            return arqueologo;
        }
    }
}