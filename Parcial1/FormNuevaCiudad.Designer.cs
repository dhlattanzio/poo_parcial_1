﻿
namespace Parcial1
{
    partial class FormNuevaCiudad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxPais = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxCiudad = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxArqueologos = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxNombreAntiguo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxPeriodoPoblacion = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxCantidadHabitantes = new System.Windows.Forms.TextBox();
            this.dateTimePickerFecha = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxPresupuesto = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBoxPais
            // 
            this.textBoxPais.Location = new System.Drawing.Point(12, 32);
            this.textBoxPais.Name = "textBoxPais";
            this.textBoxPais.Size = new System.Drawing.Size(237, 20);
            this.textBoxPais.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Pais";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Ciudad";
            // 
            // textBoxCiudad
            // 
            this.textBoxCiudad.Location = new System.Drawing.Point(12, 79);
            this.textBoxCiudad.Name = "textBoxCiudad";
            this.textBoxCiudad.Size = new System.Drawing.Size(237, 20);
            this.textBoxCiudad.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Fecha";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 157);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Arqueologo";
            // 
            // comboBoxArqueologos
            // 
            this.comboBoxArqueologos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxArqueologos.FormattingEnabled = true;
            this.comboBoxArqueologos.Location = new System.Drawing.Point(12, 173);
            this.comboBoxArqueologos.Name = "comboBoxArqueologos";
            this.comboBoxArqueologos.Size = new System.Drawing.Size(237, 21);
            this.comboBoxArqueologos.TabIndex = 8;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(15, 401);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(234, 35);
            this.button1.TabIndex = 9;
            this.button1.Text = "Agregar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 249);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Nombre antiguo";
            // 
            // textBoxNombreAntiguo
            // 
            this.textBoxNombreAntiguo.Location = new System.Drawing.Point(12, 268);
            this.textBoxNombreAntiguo.Name = "textBoxNombreAntiguo";
            this.textBoxNombreAntiguo.Size = new System.Drawing.Size(237, 20);
            this.textBoxNombreAntiguo.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 299);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(156, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Periodo poblacion activa (años)";
            // 
            // textBoxPeriodoPoblacion
            // 
            this.textBoxPeriodoPoblacion.Location = new System.Drawing.Point(12, 318);
            this.textBoxPeriodoPoblacion.Name = "textBoxPeriodoPoblacion";
            this.textBoxPeriodoPoblacion.Size = new System.Drawing.Size(237, 20);
            this.textBoxPeriodoPoblacion.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 347);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Cantidad habitantes";
            // 
            // textBoxCantidadHabitantes
            // 
            this.textBoxCantidadHabitantes.Location = new System.Drawing.Point(12, 366);
            this.textBoxCantidadHabitantes.Name = "textBoxCantidadHabitantes";
            this.textBoxCantidadHabitantes.Size = new System.Drawing.Size(237, 20);
            this.textBoxCantidadHabitantes.TabIndex = 14;
            // 
            // dateTimePickerFecha
            // 
            this.dateTimePickerFecha.Location = new System.Drawing.Point(12, 123);
            this.dateTimePickerFecha.Name = "dateTimePickerFecha";
            this.dateTimePickerFecha.Size = new System.Drawing.Size(237, 20);
            this.dateTimePickerFecha.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 202);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Presupuesto total";
            // 
            // textBoxPresupuesto
            // 
            this.textBoxPresupuesto.Location = new System.Drawing.Point(12, 221);
            this.textBoxPresupuesto.Name = "textBoxPresupuesto";
            this.textBoxPresupuesto.Size = new System.Drawing.Size(237, 20);
            this.textBoxPresupuesto.TabIndex = 17;
            // 
            // FormNuevaCiudad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(261, 447);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBoxPresupuesto);
            this.Controls.Add(this.dateTimePickerFecha);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBoxCantidadHabitantes);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxPeriodoPoblacion);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxNombreAntiguo);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.comboBoxArqueologos);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxCiudad);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxPais);
            this.Name = "FormNuevaCiudad";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FormNuevaCiudad";
            this.Load += new System.EventHandler(this.FormNuevaCiudad_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxPais;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxCiudad;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxArqueologos;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxNombreAntiguo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxPeriodoPoblacion;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxCantidadHabitantes;
        private System.Windows.Forms.DateTimePicker dateTimePickerFecha;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxPresupuesto;
    }
}