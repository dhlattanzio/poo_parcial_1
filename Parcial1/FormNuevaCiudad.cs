﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Parcial1
{
    public partial class FormNuevaCiudad : Form
    {
        private Instituto _instituto;

        public FormNuevaCiudad(Instituto instituto)
        {
            _instituto = instituto;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string pais = textBoxPais.Text;
            string ciudad = textBoxCiudad.Text;
            string nombreAntiguo = textBoxNombreAntiguo.Text;
            if (pais == "" || ciudad == "" || nombreAntiguo == "")
            {
                MessageBox.Show("No puede haber campos vacios", "Error!");
                return;
            }

            DateTime fecha = dateTimePickerFecha.Value;

            Arqueologo arqueologo = (Arqueologo)comboBoxArqueologos.SelectedItem;
            if (arqueologo == null)
            {
                MessageBox.Show("El descubrimiento debe tener un arqueologo", "Error!");
                return;
            }

            try
            {
                int presupuesto = int.Parse(textBoxPresupuesto.Text);
                int periodoPoblacionActiva = int.Parse(textBoxPeriodoPoblacion.Text);
                int cantidadHabitantes = int.Parse(textBoxCantidadHabitantes.Text);

                Descubrimiento descubrimiento = new Ciudad(pais, ciudad, fecha, nombreAntiguo,
                        periodoPoblacionActiva, cantidadHabitantes);
                descubrimiento.Arqueologo = arqueologo;
                descubrimiento.Presupuesto = presupuesto;
                _instituto.NuevoDescubrimiento(descubrimiento);
            } catch (Exception)
            {
                MessageBox.Show("Alguno de los campos numericos no es valido", "Error!");
                return;
            }

            Close();
        }

        private void FormNuevaCiudad_Load(object sender, EventArgs e)
        {
            comboBoxArqueologos.DataSource = _instituto.Arqueologos;
            dateTimePickerFecha.MaxDate = DateTime.Now;
        }
    }
}
