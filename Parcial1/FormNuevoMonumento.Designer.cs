﻿
namespace Parcial1
{
    partial class FormNuevoMonumento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxPresupuesto = new System.Windows.Forms.TextBox();
            this.dateTimePickerFecha = new System.Windows.Forms.DateTimePicker();
            this.comboBoxArqueologos = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxCiudad = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxPais = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBoxAutoridad = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxAutoridad = new System.Windows.Forms.TextBox();
            this.checkBoxJeroglificos = new System.Windows.Forms.CheckBox();
            this.listBoxDioses = new System.Windows.Forms.ListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonDiosAgregar = new System.Windows.Forms.Button();
            this.textBoxDios = new System.Windows.Forms.TextBox();
            this.buttonDiosEliminar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 199);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 13);
            this.label8.TabIndex = 28;
            this.label8.Text = "Presupuesto total";
            // 
            // textBoxPresupuesto
            // 
            this.textBoxPresupuesto.Location = new System.Drawing.Point(12, 218);
            this.textBoxPresupuesto.Name = "textBoxPresupuesto";
            this.textBoxPresupuesto.Size = new System.Drawing.Size(237, 20);
            this.textBoxPresupuesto.TabIndex = 27;
            // 
            // dateTimePickerFecha
            // 
            this.dateTimePickerFecha.Location = new System.Drawing.Point(12, 120);
            this.dateTimePickerFecha.Name = "dateTimePickerFecha";
            this.dateTimePickerFecha.Size = new System.Drawing.Size(237, 20);
            this.dateTimePickerFecha.TabIndex = 26;
            // 
            // comboBoxArqueologos
            // 
            this.comboBoxArqueologos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxArqueologos.FormattingEnabled = true;
            this.comboBoxArqueologos.Location = new System.Drawing.Point(12, 170);
            this.comboBoxArqueologos.Name = "comboBoxArqueologos";
            this.comboBoxArqueologos.Size = new System.Drawing.Size(237, 21);
            this.comboBoxArqueologos.TabIndex = 25;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 154);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Arqueologo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Fecha";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Ciudad";
            // 
            // textBoxCiudad
            // 
            this.textBoxCiudad.Location = new System.Drawing.Point(12, 76);
            this.textBoxCiudad.Name = "textBoxCiudad";
            this.textBoxCiudad.Size = new System.Drawing.Size(237, 20);
            this.textBoxCiudad.TabIndex = 21;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Pais";
            // 
            // textBoxPais
            // 
            this.textBoxPais.Location = new System.Drawing.Point(12, 29);
            this.textBoxPais.Name = "textBoxPais";
            this.textBoxPais.Size = new System.Drawing.Size(237, 20);
            this.textBoxPais.TabIndex = 19;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(146, 265);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(234, 35);
            this.button1.TabIndex = 29;
            this.button1.Text = "Agregar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkBoxAutoridad
            // 
            this.checkBoxAutoridad.AutoSize = true;
            this.checkBoxAutoridad.Location = new System.Drawing.Point(280, 56);
            this.checkBoxAutoridad.Name = "checkBoxAutoridad";
            this.checkBoxAutoridad.Size = new System.Drawing.Size(100, 17);
            this.checkBoxAutoridad.TabIndex = 39;
            this.checkBoxAutoridad.Text = "Tiene autoridad";
            this.checkBoxAutoridad.UseVisualStyleBackColor = true;
            this.checkBoxAutoridad.CheckedChanged += new System.EventHandler(this.checkBoxAutoridad_CheckedChanged_1);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(280, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 38;
            this.label5.Text = "Autoridad";
            // 
            // textBoxAutoridad
            // 
            this.textBoxAutoridad.Enabled = false;
            this.textBoxAutoridad.Location = new System.Drawing.Point(280, 95);
            this.textBoxAutoridad.Name = "textBoxAutoridad";
            this.textBoxAutoridad.Size = new System.Drawing.Size(237, 20);
            this.textBoxAutoridad.TabIndex = 37;
            // 
            // checkBoxJeroglificos
            // 
            this.checkBoxJeroglificos.AutoSize = true;
            this.checkBoxJeroglificos.Location = new System.Drawing.Point(280, 29);
            this.checkBoxJeroglificos.Name = "checkBoxJeroglificos";
            this.checkBoxJeroglificos.Size = new System.Drawing.Size(105, 17);
            this.checkBoxJeroglificos.TabIndex = 40;
            this.checkBoxJeroglificos.Text = "Tiene jeroglificos";
            this.checkBoxJeroglificos.UseVisualStyleBackColor = true;
            // 
            // listBoxDioses
            // 
            this.listBoxDioses.FormattingEnabled = true;
            this.listBoxDioses.Location = new System.Drawing.Point(280, 143);
            this.listBoxDioses.Name = "listBoxDioses";
            this.listBoxDioses.Size = new System.Drawing.Size(131, 95);
            this.listBoxDioses.TabIndex = 41;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(280, 127);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 42;
            this.label6.Text = "Dioses";
            // 
            // buttonDiosAgregar
            // 
            this.buttonDiosAgregar.Location = new System.Drawing.Point(417, 209);
            this.buttonDiosAgregar.Name = "buttonDiosAgregar";
            this.buttonDiosAgregar.Size = new System.Drawing.Size(103, 29);
            this.buttonDiosAgregar.TabIndex = 43;
            this.buttonDiosAgregar.Text = "Agregar";
            this.buttonDiosAgregar.UseVisualStyleBackColor = true;
            this.buttonDiosAgregar.Click += new System.EventHandler(this.buttonDiosAgregar_Click);
            // 
            // textBoxDios
            // 
            this.textBoxDios.Location = new System.Drawing.Point(417, 143);
            this.textBoxDios.Name = "textBoxDios";
            this.textBoxDios.Size = new System.Drawing.Size(103, 20);
            this.textBoxDios.TabIndex = 44;
            // 
            // buttonDiosEliminar
            // 
            this.buttonDiosEliminar.Location = new System.Drawing.Point(417, 174);
            this.buttonDiosEliminar.Name = "buttonDiosEliminar";
            this.buttonDiosEliminar.Size = new System.Drawing.Size(103, 29);
            this.buttonDiosEliminar.TabIndex = 45;
            this.buttonDiosEliminar.Text = "Eliminar";
            this.buttonDiosEliminar.UseVisualStyleBackColor = true;
            this.buttonDiosEliminar.Click += new System.EventHandler(this.buttonDiosEliminar_Click);
            // 
            // FormNuevoMonumento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 320);
            this.Controls.Add(this.buttonDiosEliminar);
            this.Controls.Add(this.textBoxDios);
            this.Controls.Add(this.buttonDiosAgregar);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.listBoxDioses);
            this.Controls.Add(this.checkBoxJeroglificos);
            this.Controls.Add(this.checkBoxAutoridad);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxAutoridad);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBoxPresupuesto);
            this.Controls.Add(this.dateTimePickerFecha);
            this.Controls.Add(this.comboBoxArqueologos);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxCiudad);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxPais);
            this.Name = "FormNuevoMonumento";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FormNuevoMonumento";
            this.Load += new System.EventHandler(this.FormNuevoMonumento_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxPresupuesto;
        private System.Windows.Forms.DateTimePicker dateTimePickerFecha;
        private System.Windows.Forms.ComboBox comboBoxArqueologos;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxCiudad;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxPais;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox checkBoxAutoridad;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxAutoridad;
        private System.Windows.Forms.CheckBox checkBoxJeroglificos;
        private System.Windows.Forms.ListBox listBoxDioses;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonDiosAgregar;
        private System.Windows.Forms.TextBox textBoxDios;
        private System.Windows.Forms.Button buttonDiosEliminar;
    }
}