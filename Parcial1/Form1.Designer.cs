﻿
namespace Parcial1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.listBoxArqueologos = new System.Windows.Forms.ListBox();
            this.checkBoxDifunto = new System.Windows.Forms.CheckBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.dateTimePickerDeceso = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerNacimiento = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxApellido = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxNombre = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.comboBoxDescubrimiento = new System.Windows.Forms.ComboBox();
            this.listBoxDescubrimientos = new System.Windows.Forms.ListBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.buttonEliminar = new System.Windows.Forms.Button();
            this.buttonConsultar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.listBoxArqueologos);
            this.groupBox1.Controls.Add(this.checkBoxDifunto);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.dateTimePickerDeceso);
            this.groupBox1.Controls.Add(this.dateTimePickerNacimiento);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBoxApellido);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBoxNombre);
            this.groupBox1.Location = new System.Drawing.Point(13, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(237, 426);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Arqueólogos";
            // 
            // listBoxArqueologos
            // 
            this.listBoxArqueologos.FormattingEnabled = true;
            this.listBoxArqueologos.Location = new System.Drawing.Point(7, 19);
            this.listBoxArqueologos.Name = "listBoxArqueologos";
            this.listBoxArqueologos.Size = new System.Drawing.Size(143, 95);
            this.listBoxArqueologos.TabIndex = 28;
            // 
            // checkBoxDifunto
            // 
            this.checkBoxDifunto.AutoSize = true;
            this.checkBoxDifunto.Location = new System.Drawing.Point(6, 269);
            this.checkBoxDifunto.Name = "checkBoxDifunto";
            this.checkBoxDifunto.Size = new System.Drawing.Size(60, 17);
            this.checkBoxDifunto.TabIndex = 7;
            this.checkBoxDifunto.Text = "Difunto";
            this.checkBoxDifunto.UseVisualStyleBackColor = true;
            this.checkBoxDifunto.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(156, 69);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 40);
            this.button3.TabIndex = 27;
            this.button3.Text = "Consultar";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(156, 25);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 38);
            this.button2.TabIndex = 26;
            this.button2.Text = "Eliminar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 383);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(225, 37);
            this.button1.TabIndex = 25;
            this.button1.Text = "Nuevo";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // dateTimePickerDeceso
            // 
            this.dateTimePickerDeceso.Enabled = false;
            this.dateTimePickerDeceso.Location = new System.Drawing.Point(6, 292);
            this.dateTimePickerDeceso.Name = "dateTimePickerDeceso";
            this.dateTimePickerDeceso.Size = new System.Drawing.Size(177, 20);
            this.dateTimePickerDeceso.TabIndex = 24;
            // 
            // dateTimePickerNacimiento
            // 
            this.dateTimePickerNacimiento.Location = new System.Drawing.Point(6, 234);
            this.dateTimePickerNacimiento.Name = "dateTimePickerNacimiento";
            this.dateTimePickerNacimiento.Size = new System.Drawing.Size(177, 20);
            this.dateTimePickerNacimiento.TabIndex = 23;
            this.dateTimePickerNacimiento.Value = new System.DateTime(2021, 5, 23, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 218);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Fecha de Nacimiento";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 172);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Apellido";
            // 
            // textBoxApellido
            // 
            this.textBoxApellido.Location = new System.Drawing.Point(6, 188);
            this.textBoxApellido.Name = "textBoxApellido";
            this.textBoxApellido.Size = new System.Drawing.Size(177, 20);
            this.textBoxApellido.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 127);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Nombre";
            // 
            // textBoxNombre
            // 
            this.textBoxNombre.Location = new System.Drawing.Point(6, 143);
            this.textBoxNombre.Name = "textBoxNombre";
            this.textBoxNombre.Size = new System.Drawing.Size(177, 20);
            this.textBoxNombre.TabIndex = 15;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonConsultar);
            this.groupBox2.Controls.Add(this.buttonEliminar);
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Controls.Add(this.comboBoxDescubrimiento);
            this.groupBox2.Controls.Add(this.listBoxDescubrimientos);
            this.groupBox2.Location = new System.Drawing.Point(256, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(242, 426);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Descubrimientos";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(6, 383);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(230, 37);
            this.button4.TabIndex = 26;
            this.button4.Text = "Nuevo";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // comboBoxDescubrimiento
            // 
            this.comboBoxDescubrimiento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDescubrimiento.FormattingEnabled = true;
            this.comboBoxDescubrimiento.Location = new System.Drawing.Point(6, 356);
            this.comboBoxDescubrimiento.Name = "comboBoxDescubrimiento";
            this.comboBoxDescubrimiento.Size = new System.Drawing.Size(230, 21);
            this.comboBoxDescubrimiento.TabIndex = 8;
            // 
            // listBoxDescubrimientos
            // 
            this.listBoxDescubrimientos.FormattingEnabled = true;
            this.listBoxDescubrimientos.Location = new System.Drawing.Point(6, 18);
            this.listBoxDescubrimientos.Name = "listBoxDescubrimientos";
            this.listBoxDescubrimientos.Size = new System.Drawing.Size(230, 225);
            this.listBoxDescubrimientos.TabIndex = 0;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(520, 70);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(202, 59);
            this.button5.TabIndex = 8;
            this.button5.Text = "d) Obtener el descubrimiento que haya obtenido más presupuesto en excavación";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(520, 135);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(202, 59);
            this.button6.TabIndex = 9;
            this.button6.Text = "e) Obtener el descubrimiento que haya asignado más presupuesto en materiales";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(520, 200);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(202, 59);
            this.button7.TabIndex = 10;
            this.button7.Text = "f) Obtener el descubrimiento qué menos dinero haya abonado en permisos";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(520, 265);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(202, 59);
            this.button8.TabIndex = 11;
            this.button8.Text = "g) Obtener el descubrimiento más antiguo";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(520, 330);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(202, 59);
            this.button9.TabIndex = 12;
            this.button9.Text = "h) Obtener el arqueólogo qué más descubrimientos haya realizado";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // buttonEliminar
            // 
            this.buttonEliminar.Location = new System.Drawing.Point(6, 249);
            this.buttonEliminar.Name = "buttonEliminar";
            this.buttonEliminar.Size = new System.Drawing.Size(230, 37);
            this.buttonEliminar.TabIndex = 27;
            this.buttonEliminar.Text = "Eliminar";
            this.buttonEliminar.UseVisualStyleBackColor = true;
            this.buttonEliminar.Click += new System.EventHandler(this.buttonEliminar_Click);
            // 
            // buttonConsultar
            // 
            this.buttonConsultar.Location = new System.Drawing.Point(6, 292);
            this.buttonConsultar.Name = "buttonConsultar";
            this.buttonConsultar.Size = new System.Drawing.Size(230, 37);
            this.buttonConsultar.TabIndex = 28;
            this.buttonConsultar.Text = "Consultar";
            this.buttonConsultar.UseVisualStyleBackColor = true;
            this.buttonConsultar.Click += new System.EventHandler(this.buttonConsultar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(759, 450);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker dateTimePickerDeceso;
        private System.Windows.Forms.DateTimePicker dateTimePickerNacimiento;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxApellido;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxNombre;
        private System.Windows.Forms.CheckBox checkBoxDifunto;
        private System.Windows.Forms.ListBox listBoxArqueologos;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox comboBoxDescubrimiento;
        private System.Windows.Forms.ListBox listBoxDescubrimientos;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button buttonConsultar;
        private System.Windows.Forms.Button buttonEliminar;
    }
}

